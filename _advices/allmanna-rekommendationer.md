---

layout: advice
title:  "Allmänna råd"
shortTitle: "Allmänna råd"
image: allmänna-rekommendationer.jpg
category: allmänna-råd
orginalrecept: https://www.livsmedelsverket.se/matvanor-halsa--miljo/kostrad/rad-om-bra-mat-hitta-ditt-satt

---
### Livsmedelsverket förespråkar
- Ät mer frukt och grönsaker – gärna minst 500 gram om dagen
- Ät fisk och skaldjur – gärna 2–3 gånger per vecka
- Byt till fullkorn när du äter bröd, flingor, gryn, pasta och ris
- Byt till nyttiga matfetter – välj exempelvis rapsolja eller flytande matfetter
- Byt till magra mejeriprodukter – gärna osötade och berikade med D-vitamin
- Ät mindre rött kött och chark – gärna mindre än 500 gram per vecka
- Ät mindre salt – men använd salt med jod
- Ät mindre socker – minska särskilt på söta drycker

### Övrigt
- Att vara fysisk aktiv är hälsofrämjande. Vuxna bör varje vecka vara fysiskt aktiva på måttlig intensitet i minst 150–300 minuter (Folkhälsomyndigheten, 2022).
- Att få i sig tillräckligt med järn är viktigt, speciellt som kvinna i fertil ålder. I vegetabiliska livsmedel finns järn i fullkornsprodukter, nötter, frön, torkad frukt och baljväxter. Största nivån av järn finns annars i animaliska livsmedel som i inälvs- och blodmat som lever och blodpudding, kött, ägg och skaldjur.
