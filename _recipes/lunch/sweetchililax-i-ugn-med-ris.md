---

layout: recipe
title:  "Sweet chililax i ugn med ris"
shortTitle: "Sweet chililax"
image: sweet-chilli-lax.jpeg
category: middag
orginalrecept: https://www.mathem.se/recept/sweet-chililax-i-ugn-med-ris

ingredients:
- 500 g Laxfilé Fryst
- 2 dl Creme Fraiche Naturell
- 1 dl Sweet Chili
- 4 portion Ris
- 300 g Morötter
- 2 krm Salt Fint
- 1 krm Svartpeppar Malen
- 1 msk Mat & Bak Smör

directions:
- Tina laxfileérna. Värm ugnen till 175 grader. Koka ris enligt anvisning på förpackningen.
- Lägg laxfiléerna i en ugnsfast form och krydda med salt och peppar efter smak.
- Blanda ihop creme fraiche och sweet chilisås och häll över laxen. Sätt in i ugnen ca 12 min.
- Koka upp saltat vatten. Skala och skär morötterna i stavar och koka dem ca 4-5 min. Sila av vattnet och tillsätt smör till morötterna.
- Servera sweet chililaxen tillsammans med ris och morötter.

nutrition:
    servingSize: per portion
    calories: 593
    fatContent: 25
    carbohydrateContent: 62
    proteinContent: 29

---
