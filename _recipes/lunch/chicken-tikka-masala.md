---

layout: recipe
title:  "Chicken tikka masala"
shortTitle: "Chicken tikka"
image: chicken-tikka-masala.jpeg
category: middag
orginalrecept: https://www.mathem.se/recept/snabb-chicken-tikka-masala

ingredients:
- 600 g Kycklingfilé Färsk
- 4 tsk Rapsolja
- 2 krm Salt Fint
  -2 krm Svartpeppar Malen
- 600 g Spicy Tikka Masala Sauce
- 2 dl Vatten

# Myntasås
- 1 dl Yoghurt för Matlagning
  -1 st krukor Mynta - färsk
- 4 krm Vitvinsvinäger
- 2 krm Salt Fint
- 2 krm Svartpeppar Malen

# Tillbehör
- 500 g Vitkål
- 2 tsk Rapsolja
- 1 st Gurka
- 60 g Machesallat
- 2 knäckebröd med lättmargarin

directions:
- Värm ugnen till 200 grader.
- Skär vitkålen i munsstora bitar och lägg i en ugnsfast form. Ringla över olja och krydda med lite salt. Rosta i ugnen i ca 20 min.
- Tärna kycklingen och bryn i olja runt om i en stekpanna. Krydda med lite salt och peppar.
- Tillsätt tikka masalasåsen och vatten och låt sjuda i ca 15 min.
- Myntasås- Blanda yoghurt med finhackad mynta och vitvinsvinäger. Smaka av med salt och peppar.
- Tillbehör- Hyvla långa remsor av gurkan med hjälp av en potatisskalare. Blanda med machésalladen.
- Servera chicken tikka masala med rostad vitkål och gurksallad. Ringla över lite yoghurtsås. Klart att servera.

nutrition:
  servingSize: per portion
  calories: 679
  fatContent: 26
  carbohydrateContent: 25
  proteinContent: 41

---
