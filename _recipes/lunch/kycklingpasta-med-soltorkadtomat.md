---

layout: recipe
title:  "Krämig kycklingpasta med soltorkad tomat"
shortTitle: "Krämig kycklingpasta"
image: kycklingpasta-med-soltorkadtomat.jpeg
category: middag
orginalrecept: https://www.mathem.se/recept/kramig-kycklingpasta-med-soltorkad-tomat

ingredients:
- 400 g Tagliatelle
- 600 g Kycklingfilé Färsk
- 140 g Soltorkade Tomater
- 2.5 dl Vispgrädde
- 100 g Parmesanost
- 1 msk Olivolja
- 2 st klyftor Vitlök Vanlig
- 0.5 tsk Basilika - torkad
- 1 msk Kycklingfond Buljong
- 1 krm Salt Fint
- 1 krm Svartpeppar

directions:
- Koka pastan efter anvisning på paketet.
- Skala och riv vitlök. Strimla kycklingen och de soltorkade tomaterna. Hetta upp en stor stekpanna med olja och bryn vitlök, kyckling och basilika till en fin stekyta. Tillsätt soltorkade tomater och stek ihop.
- Tillsätt fond, grädde och ½ dl av pastavattnet och låt det koka ihop i 4-5 min. Smaka av med salt och peppar.
- Sila av pastan och blanda med kycklingsåsen. Vänd ner finriven parmesanost och servera meddetsamma.

nutrition:
    servingSize: per portion
    calories: 777
    fatContent: 38
    carbohydrateContent: 53
    proteinContent: 55

---
