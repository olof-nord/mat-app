---

layout: recipe
title:  "Vattenmelonsallad med mynta och grillad halloumi"
shortTitle: "Vattenmelonsallad"
image: vattenmelonsallad.jpeg
category: middag
orginalrecept: https://www.mathem.se/recept/vattenmelonsallad-med-mynta-och-grillad-halloumi

ingredients:
- 500 g Halloumi
- 1 msk Olivolja
- 1 st Vattenmelon
- 60 g Salladslök
- 1 st krukor Mynta - färsk
- 1 st Gurka
- 1 st Lime
- 1 msk Olivolja
- 1 krm Salt Fint
- 1 krm Svartpeppar Malen

directions:
- Värm ugnen till 200 grader
- Hetta upp olja i en stekpanna och stek de hela halloumibitarna runt om på alla sidor så att de får en fin gyllene stekyta. Lägg över halloumin i en ugnsfast form och sätt in i ugnen i ca 15 min.
- Skala vattenmelonen och skär melonen i 2x2 cm bitar. Strimla salladslöken och finhacka en halv kruka mynta. Blanda melon, salladslök och mynta på ett fat.
  -Hyvla långa remsor av gurkan med hjälp av en potatisskalare. Fördela gurkskivorna runt om. Tillsätt rivet limeskal och pressa över limesaft. Ringla över olja och krydda med lite salt och peppar.
- Ta ut halloumin och skär den i skivor. Toppa melonsalladen med halloumin och dekorera med myntakvistar. Ät och njut!

nutrition:
  servingSize: per portion
  calories: 522
  fatContent: 34
  carbohydrateContent: 26
  proteinContent: 27

---
