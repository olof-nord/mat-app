---

layout: recipe
title: "Sparrispaj med parmesan och spenat"
shortTitle: "Sparrispaj"
image: sparrispaj-med-parmesan-och-spenat.jpeg
category: middag
orginalrecept: https://www.mathem.se/recept/sparrispaj-med-parmesan-och-spenat

ingredients:
# Fyllning
- 1 st Purjolök
- 2 st klyftor Vitlök Vanlig
- 125 g Sparris Färsk
- 2 msk Mat & Bak Smör
- 150 g Färsk bladspenat
- 1 krm Salt Fint
- 1 krm Svartpeppar Malen
- 300 g Pajskal

# Äggstanning
- 100 g Parmesanost
- 3 st Ägg
- 2 krm Salt Fint
- 2 krm Svartpeppar Malen
- 2 dl Creme Fraiche Naturell

# Topping
- 125 g Sparris Färsk
- 50 g Färsk bladspenat
- 100 g Ärtskott
- 50 g Parmesanost
- 1 st Citron

directions:
- Värm ugnen till 200 grader.
- Fyllning- Skölj och finhacka purjolöken. Skala och riv vitlök. Ta bort den nedersta delen på sparrisen och skär stjälkarna i ½ cm tunna bitar. Spar knopparna till toppingen.
- Smält smöret i en stekpanna och fräs purjolök, vitlök och sparris på låg värme utan att det tar färg. Tillsätt spenaten och låt den bli varm. Krydda med en nypa salt och peppar.
- Äggstanning- Riv parmesanosten. Vispa ihop ägg, creme fraiche, parmesanost, salt och peppar i en bunke.
- Fyll pajskalen med sparris och spenatfräset och fördela äggstanningen över. Grädda i ugn i ca 15-20 min.
- Topping- Koka upp en kastrull med vatten och förväll sparrisknopparna i 30 sek. Spola dem sedan kalla.
- Skär bort den nedersta delen av ärtskotten. Blanda ärtskottsknopparna med spenat och sparrisknopparna och toppa de färdiga pajerna med. Riv över lite parmesan och citronskal. Klart att servera.

nutrition:
    servingSize: per portion
    calories: 788
    fatContent: 56
    carbohydrateContent: 41
    proteinContent: 30

---
