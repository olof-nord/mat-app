---

layout: recipe
title: "Saffranskryddad fisksoppa med lax och torsk"
shortTitle: "Saffranskryddad fisksoppa"
image: saffranskryddad-fisksoppa.jpeg
category: middag
orginalrecept: https://www.mathem.se/recept/saffranskryddad-fisksoppa-med-lax-och-torsk

ingredients:
- 370 g Torskfilé
- 300 g Laxfilé
- 1 st klyftor Vitlök Vanlig
- 1 st Purjolök
- 1 msk Olivolja
- 0.5 g Saffran
- 1 tsk Timjan - torkad
- 1 tsk Basilika - torkad
- 2.5 dl Matlagningsvin Vitt
- 2 dl Vatten
- 3 msk Fiskfond Buljong
- 2.5 dl Matlagningsgrädde
- 1 dl Creme Fraiche Naturell
- 1 krm Salt
- 1 krm Svartpeppar Malen
- 150 g Handskalade Räkor

directions:
- Skär fisken i 3 cm stora kuber.
- Skala och riv vitlök. Skölj och strimla purjolök.
- Hetta upp olja i en kastrull och fräs vitlök och purjolök med saffran, timjan och basilika under omrörning i ca 2-3 min.
- Tillsätt vitt vin och låt sjuda i ca 2 min så att vinet reduceras.
- Tillsätt vatten, fond, grädde och creme fraiche och låt soppan sjuda under lock i ca 10 min. Smaka av soppan med salt och peppar.
- Tillsätt fiskbitarna och låt de sjuda i soppan i ca 4-5 min. Tillsätt räkor och servera meddetsamma. Njut av en fantastisk fisksoppa.

nutrition:
    servingSize: per portion
    calories: 669
    fatContent: 47
    carbohydrateContent: 10
    proteinContent: 41

---
