---

layout: recipe
title: "Vegetarisk lasagne"
shortTitle: "Vegetarisk lasagne"
image: vegetarisk-lasagne.jpeg
category: middag
orginalrecept: https://www.mathem.se/recept/vegetarisk-lasagne

ingredients:
- 1 st Gul lök
- 1.5 st klyftor Vitlök Vanlig
- 1 msk Olivolja
- 268 g Vegetarisk Färs
- 1 msk Tomatpuré
- 1 dl Vatten
- 4 dl Krossade Tomater
- 1 st tärningar Grönsaksbuljong
- 1 tsk Basilika - torkad
- 0.5 tsk Flingsalt
- 0.5 tsk Svartpeppar Malen
- 335 g Lasagneplattor

# Ostsås
- 6 dl Standardmjölk
- 0.5 dl Vetemjöl
- 1.5 msk Mat & Bak Smör
- 1 krm Vitpeppar Malen
- 1 tsk Flingsalt
- 1 dl Riven Gratängost

directions:
- Finhacka lök och vitlök och fräs i olivolja. Häll i färsen och fräs i några minuter. Tillsätt tomatpuré. Slå på lite vatten. Häll på krossade tomater och smula i buljong. Krydda och koka 15 minuter.
- Ostsås- Smält smöret, rör i mjöl och späd med lite mjölk i taget samtidigt som du vispar. Låt småkoka i 5 minuter under omrörning. Krydda. Rör i osten.
- Varva färssås med ostsås och lasagneplattor. Börja med färssås och sluta med ostsås. Strö över lite riven ost. Gratinera i ca 20 minuter 250 grader.

nutrition:
    servingSize: per portion
    calories: 804
    fatContent: 35
    carbohydrateContent: 77
    proteinContent: 41

---
