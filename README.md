# mat-app

This is a recipe recommendation and information project.

It is based on the excellent [chowdown](https://github.com/clarklab/chowdown) project.

## Writing a Recipe

The recipes are stored in the collection "Recipes" (the folder [_recipes](_recipes)).

They are written in Markdown and contain a few special sections:

- The frontmatter, which contains:
- Title, Image, and Layout (which is "recipe")
- Ingredients (a list of things in the dish)
- Directions (a list of steps for the dish)
- Body content (for intros, stories, written detail)
- Nutrition information

If you need help with Markdown, here's a [handy cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

## Resources

- [Jekyll Cheat Sheet](https://cloudcannon.com/community/jekyll-cheat-sheet/)
- [Serving Static Content](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/)