FROM debian:sid AS builder

RUN apt-get update && apt-get install -y ruby3.1 ruby3.1-dev ruby-bundler bison dpkg-dev libgdbm-dev

ARG BASEURL=http://localhost:8080

ENV TZ=Europe/Stockholm
ENV JEKYLL_ENV=production
ENV LANG=C.UTF-8

# This is taken from the official Ruby Dockerfile
# https://github.com/docker-library/ruby/blob/master/3.1/bullseye/Dockerfile
ENV GEM_HOME=/usr/local/bundle
ENV BUNDLE_SILENCE_ROOT_WARNING=1
ENV BUNDLE_APP_CONFIG="$GEM_HOME"
ENV PATH=$GEM_HOME/bin:$PATH

WORKDIR /root/src

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .
RUN bundle exec jekyll build --baseurl ${BASEURL}

FROM registry.gitlab.com/olof-nord/mat-app/nginx:latest

ENV TZ=Europe/Stockholm

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /root/src/_site /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
