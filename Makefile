SHELL := /bin/bash -o pipefail

APP_VERSION := 1.0.0

.PHONY: build
build:
	@echo "Building and pushing docker image"
	docker buildx build \
		--platform=linux/riscv64 \
		--tag registry.gitlab.com/olof-nord/mat-app:$(APP_VERSION) \
		--build-arg BASEURL="https://mat-app.top" \
		--push \
		.
